import React from 'react';
import './Portfolio.css';
import {Swiper, SwiperSlide} from 'swiper/react';
import 'swiper/css'
import yateam from '../../img/WebTeam.jpeg';
import cars from '../../img/WebChallenge.jpeg';
// import upnormal from '../../img/Upnormal.jpg';
import UXD from '../../img/uxd.jpg';


const Portfolio = () => {
  return (
    <div className="portfolio">
        {/* heading */}
        <span>Recent Projects</span>
        <span>Portfolio</span>

        {/* slider */}
        <Swiper
        spaceBetween={0.25}
        slidesPerView={3}
        grabCursor={true}
        className='portfolio-slider'
        >
            <SwiperSlide>
                <img src={yateam} alt=''/>
            </SwiperSlide>
            <SwiperSlide>
                <img src={cars} alt=''/>
            </SwiperSlide>
            <SwiperSlide>
                <img src={UXD} alt=''/>
            </SwiperSlide>



        </Swiper>
    </div>
  )
};

export default Portfolio;