import React from 'react'
import Card from '../Card/Card'
import './Skill.css'
import HeartEmoji from '../../img/heartemoji.png';
import Glasses from '../../img/glasses.png';
import Humble from '../../img/humble.png'
import CV from './CV.pdf';

const Skill = () => {
  return (
    <div className="skills">
        {/* <left side> */}
        <div className="awesome">
            <span>My Awesome</span>
            <span>Skills</span>
            <span>
                Saya seorang Graphic Designer, Frontend Web Developer, UI/UX Designer dan Video Editor.
                <br/>
                Saya mampu beradaptasi dengan cepat, mampu bekerja bersama tim, dan mandiri.
            </span>
            <a href={CV} download>
                <button className="button s-button">Download CV</button>
            </a>
            <div className="blur s-blur1" style={{background: "#ABF1FF94"}}></div>
        </div>
        {/* <right side> */}
        <div className="cards">
            {/* <first card> */}
            <div style={{left: '14rem'}}>
                <Card
                emoji = {HeartEmoji}
                heading = {'Design'}
                detail = {"Figma, Adobe Photoshop, Adobe Illustrator, CorelDraw"}
                />
            </div>
            {/* <second card> */}
            <div style={{top:'12rem', left: '-4rem'}}>
                <Card
                emoji = {Glasses}
                heading = {'Developer'}
                detail = {"Html, Css, Javascript, React"}
                />
            </div>
            {/* <third card> */}
            <div style={{top:'19rem', left: '12rem'}}>
                <Card
                emoji = {Humble}
                heading = {'Video Editor'}
                detail = {"Adobe Premiere, Capcut"}
                />
            </div>
            <div className="blur s-blur2" style={{background: "var(--purple)"}}></div>
        </div> 
    </div>
  )
}

export default Skill