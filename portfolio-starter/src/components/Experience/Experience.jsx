import React from 'react';
import './Experience.css';


const Experience = () => {
  return (
    <div className="experience">
        <div className="build">
            <div className="circle">12+</div>
            <span>Best</span>
            <span>Design</span>
        </div>
        <div className="build">
            <div className="circle">6+</div>
            <span>Completed</span>
            <span>Projects</span>
        </div>
        <div className="build">
            <div className="circle">10+</div>
            <span>Job</span>
            <span>Organizations</span>
        </div>
    </div>
  )
};

export default Experience;