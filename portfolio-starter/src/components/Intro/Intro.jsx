import React from "react";
import './Intro.css';
import Gitlab from '../../img/gitlab.png';
import Linkedin from '../../img/linkedin.png';
import Instagram from '../../img/instagram.png';
import Vector1 from '../../img/Vector1.png'
import Vector2 from '../../img/Vector2.png'
import boy1 from '../../img/boy1.png'
import thumbup from '../../img/thumbup.png'
import Crown from '../../img/crown.png'
import glassesemoji from '../../img/glassesimoji.png'
import FloatingDiv from "../FloatingDiv/FloatingDiv.jsx";


const Intro = () => {
    return(
        <div className="intro">
            <div className="i-left">
                <div className="i-name">
                    <span> Halo, Saya</span>
                    <span> M Fadhli Rahmansyah</span>
                    <span> Saya merupakan mahasiswa jurusan Teknik Informatika, Institut Teknologi Indonesia.
                        Saya seorang Graphic Designer, Frontend Web Developer, dan UI/UX Designer</span>
                </div>
                <button className="button i-button">
                    Hire Me
                </button>
                <div className="i-icons">
                    <a href="https://gitlab.com/jackfadhli32" ><img src={Gitlab} alt=""/></a>
                    <a href="https://www.linkedin.com/in/m-fadhli-rahmansyah-a48449227/" ><img src={Linkedin} alt=""/></a>
                    <a href="https://www.instagram.com/bj_fadhli/" ><img src={Instagram} alt=""/></a>
                </div>
                

            </div>
            <div className="i-right">
                <img src={Vector1} alt=""/>
                <img src={Vector2} alt=""/>
                <img src={boy1} alt=""/>
                <img src={glassesemoji} alt=""/>
                <div style={{top:'-4%', left:'68%'}}>
                    <FloatingDiv image={Crown} txt1='Web' txt2='Developer'/>
                </div>
                <div style={{top:'18rem', left:'0rem'}}>
                    <FloatingDiv image={thumbup} txt1='Best Design' txt2='Award'/>
                </div>

                <div className="blur" style={{ background: "rgb(238 210 255)" }}></div>
            <div
            className="blur"
            style={{
                background: "#C1F5FF",
                top: "17rem",
                width: "21rem",
                height: "11rem",
                left: "-9rem",
            }}
            ></div>
            </div>
        </div>
    )
};

export default Intro;